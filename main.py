import sys

import pygame as pg

import boarding
import course_info_entry
import course_selection_edit
import course_selection_play
import editor
import main_menu
import prepare
from state_engine import Game

states = {
    "MAIN_MENU": main_menu.MainMenu(),
    "COURSE_SELECT_PLAY": course_selection_play.CourseSelectPlay(),
    "NEW_COURSE": course_info_entry.CourseInfoEntry(),
    "EDITOR": editor.Editor(),
    "COURSE_SELECT_EDIT": course_selection_edit.CourseSelectEdit(),
    "BOARDING": boarding.Boarding()
}
game = Game(prepare.SCREEN, states, "MAIN_MENU")
game.run()
pg.quit()
sys.exit()
