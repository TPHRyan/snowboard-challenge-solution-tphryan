import os
import json

import pygame as pg

import obstacles
import prepare
import tools
from course import Course
from labels import Button
from labels import ButtonGroup
from obstacle_bar import ObstacleBar
from state_engine import GameState

OFF_WHITE = (242, 255, 255)


class Editor(GameState):
    """Allows the user to edit a course."""
    def __init__(self):
        super(Editor, self).__init__()
        self.background_color = pg.Color(*OFF_WHITE)
        self.next_state = "MAIN_MENU"
        self.held_object = None
        self.new_objects = pg.sprite.Group()
        self.scroll_speed = .25
        self.course = None
        self.view_center = []

        def button_callback(obstacle):
            self.held_object = obstacle
        self.obstacle_bar = ObstacleBar(callback=button_callback)

    def startup(self, persistent):
        """Creates a Course object from the previously selected JSON file."""
        self.persist = persistent
        name = self.persist["course_name"]
        filepath = os.path.join("resources", "courses", "{}.json".format(name))
        with open(filepath, "r") as f:
            course_info = json.load(f)
        self.course = Course(course_info)
        self.view_center = list(self.course.view_rect.center)

    def add_obstacle(self, screen_pos):
        if self.held_object is not None:
            screen_x, screen_y = screen_pos
            offset_x, offset_y = self.course.view_rect.topleft
            actual_x, actual_y = actual_pos = (screen_x + offset_x,
                                               screen_y + offset_y)
            if (self.course.map_size[0] >= actual_x >= 0 and
                    self.course.map_size[1] >= actual_y >= 0):
                self.new_objects.add(self.held_object(midbottom=actual_pos))

    def remove_obstacle(self, screen_pos):
        obs = self.course.obstacle_at_pos(screen_pos, relative_to_view=True,
                                          add_objs=self.new_objects)
        if obs is None:
            print('nothing to remove at {}'.format(screen_pos))
        else:
            obs.kill()

    def commit_changes(self):
        for obj in self.new_objects:
            self.course.obstacles.add(obj)
        self.new_objects.empty()

    def save_to_json(self):
        """Saves location of all course objects to be loaded for future use."""
        self.commit_changes()
        course_info = {
            "map_name": self.course.map_name,
            "map_size": self.course.map_size,
            "obstacles":
                [[x.name, x.rect.midbottom] for x in self.course.obstacles]
        }

        filename = "{}.json".format(self.course.map_name)
        filepath = os.path.join("resources", "courses", filename)
        with open(filepath, "w") as f:
            json.dump(course_info, f)
        
    def get_event(self, event):
        if event.type == pg.QUIT:
            self.save_to_json()
            self.done = True
        elif event.type == pg.KEYUP:
            if event.key == pg.K_ESCAPE:
                self.save_to_json()
                self.done = True
        elif event.type == pg.MOUSEBUTTONUP:
            mouse_pos = event.pos
            if self.obstacle_bar.rect.collidepoint(mouse_pos):
                self.obstacle_bar.click(mouse_pos)
            else:
                if event.button == 1:
                    self.add_obstacle(mouse_pos)
                elif event.button == 3:
                    self.remove_obstacle(mouse_pos)

    def scroll(self, dt, mouse_pos):
        """Move the view rect when the mouse is at the edge of the screen."""
        keys = pg.key.get_pressed()
        speed = self.scroll_speed * dt
        x, y = mouse_pos
        w, h = prepare.SCREEN_SIZE
        if x < 20 or keys[pg.K_LEFT]:
            self.view_center[0] -= speed
        elif x > w - 20 or keys[pg.K_RIGHT]:
            self.view_center[0] += speed
        if y < 20 or keys[pg.K_UP]:
            self.view_center[1] -= speed
        elif y > h - 20 or keys[pg.K_DOWN]:
            self.view_center[1] += speed  
        self.course.view_rect.center = self.view_center
        
    def update(self, dt):
        mouse_pos = pg.mouse.get_pos()
        self.scroll(dt, mouse_pos)
        self.obstacle_bar.update(dt, mouse_pos)
        
    def draw(self, surface):
        surface.fill(self.background_color)
        self.course.draw(surface)
        for obj in sorted(self.new_objects, key=lambda x: x.collider.bottom):
            offset_x = -self.course.view_rect.left
            offset_y = -self.course.view_rect.top
            obj.draw(surface, (offset_x, offset_y))
        if self.held_object is not None:
            mouse_pos = pg.mouse.get_pos()
            obj = self.held_object(midbottom=mouse_pos)
            obj.draw(surface, (0, 0))
        self.obstacle_bar.draw(surface)
