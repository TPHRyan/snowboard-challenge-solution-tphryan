import obstacles
import prepare
import pygame.sprite as sprite
from pygame import Rect
from pygame import SRCALPHA
from pygame import Surface

# Hardcoded because let's not rework the entire codebase to suit not hardcoding
OBSTACLES = [
    obstacles.Tree,
    obstacles.Rock,
    obstacles.RightGate,
    obstacles.LeftGate,
    obstacles.Jump,
    obstacles.GreenSign,
    obstacles.BlueSign,
    obstacles.BlackSign
]


class ObstacleBar(sprite.Sprite):
    Y_OFFSET = 24

    def __init__(self, callback=lambda x: 0, *args, **kwargs):
        super(ObstacleBar, self).__init__()

        self.image = None
        self.rect = None
        self.has_mouse = False
        self.padding_px = 0
        self.callback = callback
        self.icons = sprite.Group()
        self.prepare(prepare.GFX['icon-strip'],
                     prepare.GFX['icon-strip-selected'],
                     *args, **kwargs)
        self.render()

    def calculate_size(self, icon_size, icon_count):
        return (
            (icon_size * icon_count) + (self.padding_px * (icon_count - 1)),
            icon_size
        )

    def prepare(self, icon_strip, icon_strip_selected, *args, **kwargs):
        icon_rect = icon_strip.get_rect()
        icon_size = icon_rect.height
        icon_count = icon_rect.width // icon_size
        self.padding_px = icon_size // 8
        for i in range(0, icon_count):
            self.icons.add(
                ObstacleIcon(icon_strip, icon_strip_selected,
                             i, icon_size, self.padding_px,
                             callback=self.callback, obstacle=OBSTACLES[i]))
        self.image = Surface(self.calculate_size(icon_size, icon_count), *args,
                             flags=SRCALPHA, **kwargs)

    def create_draw_rect(self, surface):
        surf_width = surface.get_rect().width
        self_width, _ = self_size = self.image.get_rect().size
        self.rect = Rect(
            (surf_width // 2 - (self_width // 2), self.Y_OFFSET),
            self_size
        )

    def render(self, selected=None):
        self.image.fill((0, 0, 0, 0))
        self.icons.draw(self.image)

    def update(self, dt, mouse_pos, *args, **kwargs):
        if not self.rect:
            return
        if self.rect.collidepoint(mouse_pos):
            self.has_mouse = True
            self.update_icons(dt, mouse_pos)
        elif self.has_mouse:
            self.has_mouse = False
            self.update_icons(dt, mouse_pos)

    def update_icons(self, dt, mouse_pos):
        x, y = mouse_pos
        rel_pos = x - self.rect.left, y - self.rect.top
        self.icons.update(dt, rel_pos)
        self.render()

    def click(self, mouse_pos):
        x, y = mouse_pos
        rel_pos = x - self.rect.left, y - self.rect.top
        for icon in self.icons:
            icon.click(rel_pos)

    def draw(self, surface, *args, **kwargs):
        if not self.rect:
            self.create_draw_rect(surface)
        surface.blit(self.image, self.rect)


class ObstacleIcon(sprite.Sprite):
    def __init__(self, icon_sheet, active_icon_sheet, icon_no,
                 icon_size, padding_px, callback=lambda x: 0,
                 obstacle=obstacles.Obstacle, *groups, **kwargs):
        super(ObstacleIcon, self).__init__(*groups)
        self.callback = callback
        self.obstacle = obstacle

        sheet_offset = icon_no * icon_size
        sheet_area = (sheet_offset, 0, icon_size, icon_size)
        self.inactive_image = Surface((icon_size, icon_size), flags=SRCALPHA)
        self.inactive_image.blit(icon_sheet, (0, 0), sheet_area)
        self.active_image = Surface((icon_size, icon_size), flags=SRCALPHA)
        self.active_image.blit(active_icon_sheet, (0, 0), sheet_area)
        self.image = self.inactive_image

        bar_offset = icon_no * icon_size + icon_no * padding_px
        self.rect = Rect(bar_offset, 0, icon_size, icon_size)

    def click(self, mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            self.callback(self.obstacle)

    def update(self, dt, mouse_pos, *args, **kwargs):
        if self.rect.collidepoint(mouse_pos):
            self.image = self.active_image
        else:
            self.image = self.inactive_image
